import java.util.Arrays;

public class Group {
    private String name;
    private Student[] students;

    public Group() {
        students = new Student[Const.copacityGroup];
    }

    public Group(String name) {
        students = new Student[Const.copacityGroup];
        this.name = name;
        //this.students = students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student[] getStudents() {
        return students;
    }

    @Override
    public String toString() {
        return "Group\n" +
                "name='" + name + '\n' + "Students: \n" + Arrays.toString(students) + "}\n";
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }
}
