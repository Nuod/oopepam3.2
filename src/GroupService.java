public class GroupService implements Service<Student> {
    @Override
    public Student[] Add(Student[] a, Student b) {
        for (int i = 0;i<Const.copacityGroup ;i++)
        {
            if (a[i] == null)
            {
                a[i] = b;
                return a;
            }
        }
        return a;
    }

    @Override
    public int Find(Student[] a, Student b) {
        for (int i = 0;i<Const.copacityGroup ;i++)
        {
            if (a[i].equals(b))
            {
                return i;
            }
        }
        return -1;

    }

    @Override
    public Student[] del(Student[] a, Student b) {
        for (int i = 0;i<Const.copacityGroup ;i++)
        {
            if (a[i].equals(b))
            {
                a[i] = null;
                return a;
            }
        }
        return a;
    }
    public int CountOtlich(Group group)
    {
        int count = 0;
        MarksCalculationsService marksCalculationsService = new MarksCalculationsService();
        for (int i = 0; i<Const.copacityGroup;i++)
        {
            if((marksCalculationsService.CalcAveMarks(group.getStudents()[i].getMarks()) - 5.0) > -0.01)
            {
                count++;
            }

        }
        return count;
    }

    public int CountLosers(Group group)
    {
        MarksCalculationsService marksCalculationsService = new MarksCalculationsService();
        int count = 0;
        for (int i = 0; i<Const.copacityGroup;i++) {
            if (marksCalculationsService.CheckMark(group.getStudents()[i].getMarks(), 2)) {
                count++;
            }
        }
        return count;
    }

}
