import java.util.Arrays;

public class StudentProgress {
    private  Integer[] marks;

    public StudentProgress() {
        marks = new Integer[Const.copacityMarks];
    }

    public StudentProgress(Integer[] marks) {
        this.marks = new Integer[Const.copacityMarks];
        for (int i = 0;i<marks.length;i++)
        {
           this.marks[i] = marks[i];
        }

    }

    public Integer[] getMarks() {
        return marks;
    }

    public void setMarks(Integer[] marks) {
        this.marks = marks;
    }

    @Override
    public String toString() {
        MarksCalculationsService marksCalculationsService = new MarksCalculationsService();
        return  "marks=" + Arrays.toString(marks) +
                '/' + marksCalculationsService.CalcAveMarks(this);
    }
}
