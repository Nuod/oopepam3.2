public class SudentsMarksService implements Service<Integer> {
    @Override
    public Integer[] Add(Integer[] a, Integer b) {
        for (int i = 0;i<Const.copacityMarks;i++)
        {
            if (a[i] == null)
            {
                a[i] = b;
                return a;
            }
        }
        return a;
    }

    @Override
    public int Find(Integer[] a, Integer b) {
        for (int i = 0;i<Const.copacityMarks;i++)
        {
            if (a[i].equals(b))
            {

                return i;
            }
        }
        return -1;
    }

    @Override
    public Integer[] del(Integer[] a, Integer b) {
        for (int i = 0;i<Const.copacityMarks;i++)
        {
            if (a[i].equals(b))
            {
                a[i] = null;
                return a;
            }
        }
        return a;
    }
}
