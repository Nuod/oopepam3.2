public class MarksCalculationsService {

    public double CalcAveMarks(StudentProgress studentProgress)
    {
        int count = 0;
        double res = 0;
        for (int i = 0; i<Const.copacityMarks;i++)
        {
            if (studentProgress.getMarks()[i] != null) {
                res += studentProgress.getMarks()[i];
                count++;
            }
            }
        return res/count;
    }
    public boolean CheckMark(StudentProgress studentProgress, Integer mark)
    {
        for (int i = 0;i<Const.copacityMarks;i++ )
        {
            if ((studentProgress.getMarks()[i] != null) && (studentProgress.getMarks()[i].equals(mark)))
            {
                return true;
            }
        }
        return false;
    }
    public  double CalcAveMarksGroupe(Group group)
    {
        double res = 0;
        int count = 0;
        for (int i = 0;i<Const.copacityGroup;i++)
        {
            if (group.getStudents()[i] != null) {
                res += this.CalcAveMarks(group.getStudents()[i].getMarks());
                count++;
            }
        }
        return res/count;
    }
}
