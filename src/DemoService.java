import java.util.Arrays;

public class DemoService {
    public void Start()
    {
        Group group = new Group("SOX");
        Service groupService = new GroupService();
        groupService.Add(group.getStudents(),new Student(group,"Feda", new StudentProgress(new Integer[]{5,4,3,4})));
        groupService.Add(group.getStudents(),new Student(group,"Kata", new StudentProgress(new Integer[]{5,5,5,5,5,5,5})));
        groupService.Add(group.getStudents(),new Student(group,"Boris", new StudentProgress(new Integer[]{2,4})));
        groupService.Add(group.getStudents(),new Student(group,"Anton", new StudentProgress(new Integer[]{5,3,3,4,4})));
        groupService.Add(group.getStudents(),new Student(group,"Who?", new StudentProgress(new Integer[]{})));
        MarksCalculationsService marksCalculationsService = new MarksCalculationsService();
        //System.out.println(group);
        System.out.println("Aveg Marks Group " + marksCalculationsService.CalcAveMarksGroupe(group));
        System.out.println( "Otlich "+ ((GroupService) groupService).CountOtlich(group));
        System.out.println("Lose " + ((GroupService) groupService).CountLosers(group));

    }
}
