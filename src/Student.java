public class Student {
    private Group group;
    private  String name;
    private StudentProgress marks;

    public Student(Group group, String name, StudentProgress marks) {
        this.group = group;
        this.name = name;
        this.marks = marks;
    }

    public Student() { }

    @Override
    public String toString() {
        return "Student\n" + "name='" + name +" " + marks + "\n";
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StudentProgress getMarks() {
        return marks;
    }

    public void setMarks(StudentProgress marks) {
        this.marks = marks;
    }
}
